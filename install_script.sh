#!/bin/bash

# Define variables for the required services and packages
apache_service="httpd"
php_package="php"
php_fpm_service="php-fpm"
epel_release_url="https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm"
remi_release_url="https://rpms.remirepo.net/enterprise/remi-release-8.rpm"
git_package="git.x86_64"
boost_devel_package="boost-devel"
web_root="/var/www/html"
app_repo_url="https://gitlab.lcsb.uni.lu/CBG/transsynw.git"
temp_dir="/tmp/deploy"
R_VERSION="4.4.1"
rstudio_install_script_url="https://raw.githubusercontent.com/rstudio/r-builds/master/install.sh"
PHP_INI_PATH="/etc/php.ini"

# Function to handle errors
handle_error() {
    local error_message=$1
    local error_code=$2
    echo -e "\033[48;5;235;38;5;136m\033[38;5;255;38;5;255m"
    echo -e "   Error: $error_message"
    echo -e "   ________________________________________________________________"
    echo -e "                                                                                                                                  "
    echo -e "      Exiting with error code $error_code.                                                                   "
    echo -e "   _______________________________________________________________ \033[0m"
    exit $error_code
}

# Check if the system is Rocky Linux 8
system_version_name=$(cat /etc/os-release | grep '^NAME=' | awk -F'=' '{print $2}' | tr -d '"')
system_version=$(cat /etc/os-release | grep '^VERSION_ID=' | awk -F'=' '{print $2}' | tr -d '"')
if [ "$system_version_name" != "Rocky Linux" ] || ! [[ "$system_version" =~ ^8 ]]; then
    handle_error "This script only supports Rocky Linux 8." 1
fi

# Display a welcome message with the server version
echo -e "\033[48;5;235;38;5;136m\033[38;5;255;38;5;255m"
echo -e "   Welcome to your Rocky Linux server version $system_version_name $system_version!"
echo -e "   ________________________________________________________________"
echo -e "                                                                                                                                  "
echo -e "      Installing transsynw : PHP 8, GCC 8 and R 4.4.1 environment...                       "
echo -e "   _______________________________________________________________ \033[0m"

# Install required dependencies
sudo dnf -y install $epel_release_url || handle_error "Failed to install EPEL repository." $?
sudo dnf -y install $remi_release_url || handle_error "Failed to install Remi repository." $?
sudo dnf -y install $php_package || handle_error "Failed to install PHP package." $?
sudo dnf -y install $boost_devel_package || handle_error "Failed to install boost devel package." $?
sudo dnf -y install $git_package || handle_error "Failed to install Git package." $?

# Enable and start required services
sudo systemctl enable $apache_service || handle_error "Failed to enable Apache service." $?
sudo systemctl start $apache_service || handle_error "Failed to start Apache service." $?
sudo systemctl enable $php_fpm_service || handle_error "Failed to enable PHP-FPM service." $?
sudo systemctl start $php_fpm_service || handle_error "Failed to start PHP-FPM service." $?

# Create temporary directory if it doesn't exist
sudo mkdir -p $temp_dir || handle_error "Failed to create temporary directory." $?

# Clone application repository and deploy the application
app_dir="$temp_dir/$(basename $app_repo_url .git)"
sudo rm -rf $app_dir || handle_error "Failed to remove existing files in application directory." $?
sudo git clone $app_repo_url $app_dir || handle_error "Failed to clone application repository." $?
sudo cp -rf $app_dir/* $web_root || handle_error "Failed to copy application files to web root." $?
sudo chown -R apache:apache $web_root || handle_error "Failed to change ownership of web root directory." $?

# Restart required services
sudo systemctl restart $php_fpm_service || handle_error "Failed to restart PHP-FPM service." $?
sudo systemctl restart $apache_service || handle_error "Failed to restart Apache service." $?

# Download and install RStudio using the official installation script
sudo wget $rstudio_install_script_url
sudo bash install.sh -i ${R_VERSION} -y
sudo rm -rf install.sh

# Make R and Rscript generally available
sudo ln -sf /opt/R/${R_VERSION}/bin/R /usr/local/bin/R
sudo ln -sf /opt/R/${R_VERSION}/bin/Rscript /usr/local/bin/Rscript

# Configure local CRAN and Bioconductor mirror
cat <<EOF | sudo tee /opt/R/${R_VERSION}/lib/R/etc/Rprofile.site
# set local CRAN and bioconductor mirrors
local({r <- getOption("repos")
       r["CRAN"] <- "https://repomanager.lcsb.uni.lu/repository/r-main/"
       options(repos=r)
       options(BioC_mirror="https://repomanager.lcsb.uni.lu/repository/r-bioc")
EOF

# Download Bioconductor config.yaml and set the location
wget https://bioconductor.org/config.yaml
sudo mv config.yaml /opt/R/${R_VERSION}/lib/R/etc/bioconductor-config.yaml

# Update Rprofile.site with the Bioconductor config.yaml location
cat <<EOF | sudo tee -a /opt/R/${R_VERSION}/lib/R/etc/Rprofile.site
       options(BIOCONDUCTOR_CONFIG_FILE = "file:///opt/R/4.4.1/lib/R/etc/bioconductor-config.yaml")
})
EOF


# Install required R packages
sudo echo 'install.packages(c("Rcpp", "tibble", "dplyr", "gtools","stringr","purrr"))' | /usr/local/bin/R --save


# Update PHP configuration settings in php.ini
sudo sed -i "s/max_execution_time = .*/max_execution_time = 1000/" "$PHP_INI_PATH"
sudo sed -i "s/max_input_time = .*/max_input_time = 10000/" "$PHP_INI_PATH"
sudo sed -i "s/memory_limit = .*/memory_limit = 2048M/" "$PHP_INI_PATH"
sudo sed -i "s/post_max_size = .*/post_max_size = 49999M/" "$PHP_INI_PATH"
sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 49999M/" "$PHP_INI_PATH"

# Restart services
sudo systemctl restart $apache_service 
sudo systemctl restart $php_fpm_service

# Display success message
echo -e "\033[48;5;235;38;5;136m\033[38;5;255;38;5;255m"
echo -e "   transsynw installation with PHP 8, GCC 8 and R 4.4.1 environment completed successfully!"
echo -e "   ________________________________________________________________"
echo -e "                                                                                                                                 "
echo -e "      Your Rocky Linux server is ready for use with transsynw.                                 "
echo -e "   _______________________________________________________________\033[0m"
