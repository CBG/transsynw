#!/bin/bash
STARTPOP=$1
INPUT_FILE=$2
INPUT_FILE2=$3
SUBPOPULATIONS=$4
EMAIL=$5
SPECIES=$6
ORGNAME=$7
FILEBASE=`date +"%Y%m%d%H%M%S"`
SUFFIX=$((1 + RANDOM % 99))
OUTPUT_DIR=${FILEBASE}_${SUFFIX}
export PATH="/usr/libexec/gcc/x86_64-redhat-linux/8/:$PATH"
# ------------------
echo $INPUT_FILE $SUBPOPULATIONS $OUTPUT_DIR $INPUT_FILE2 $STARTPOP $SPECIES $ORGNAME
mkdir $OUTPUT_DIR
#date >> $OUTPUT_DIR/time.txt
/usr/local/bin/Rscript code/SynergisticCore_noExpCutoff_sizeNormalization.R $INPUT_FILE $SUBPOPULATIONS $OUTPUT_DIR $INPUT_FILE2 $STARTPOP $SPECIES $ORGNAME
#date >> $OUTPUT_DIR/time.txt
zip -r $OUTPUT_DIR.zip $OUTPUT_DIR
info=${7%%::*}
mail -a $OUTPUT_DIR.zip -s "TransSynW analysis completed" $EMAIL <<< "Dear user,

Please find in the attachments the compressed folder with the results of your analysis to $info.
If you have any questions or issues with the results, please do not hesitate to contact us by replying to this email.

Thank you very much for choosing TransSynW.

Best regards,
TransSynW team"
#to change email name go to /etc/passwd
# ------------------

rm $OUTPUT_DIR.zip
rm -rf $OUTPUT_DIR
rm $INPUT_FILE
rm $INPUT_FILE2
rm $STARTPOP
